package WordPress.pages;

import WordPress.base_class;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class home  extends base_class {
    public home(WebDriver home_driver){
        driver = home_driver;
        PageFactory.initElements(driver,home.class);
    }

    //Defining page Element
    @FindBy(xpath = "//*[@class='menu-login']/a[@id='navbar-login-link']")
    private WebElement login_link;
    @FindBy(xpath = "//li/a[@id='navbar-websites-link']")
    private WebElement website_link;

    //Method to click login button
    public void clickElement(){
        login_link.click();
    }
    public void websiteLinkClick(){
        website_link.click();
    }
}
