package WordPress;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.*;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.Properties;

public class base_class {

    public static WebDriver driver = null;

    @BeforeSuite

    public void openBrowser() throws Exception {
        // Initializing .property file
        File file = new File(System.getProperty("user.dir")+"/config/configurations.properties");
        FileInputStream fileInput = new FileInputStream(file);
        Properties prop = new Properties();
        prop.load(fileInput);

        // Initializing webdriver
        System.out.println("Statring program...");
        System.setProperty(prop.getProperty("driverName"), prop.getProperty("driverPath"));
        driver = new ChromeDriver();
        driver.get(prop.getProperty("site"));
        driver.manage().window().fullscreen();
    }

    @AfterSuite
    public void closeBrowser() throws Exception{
        driver.quit();
    }
}
